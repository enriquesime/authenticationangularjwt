import { Component, OnInit } from '@angular/core';
import { TokenService } from './auth/token.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private roles: string[];
  private authority: string;

  constructor(private tokenService: TokenService) { }

  ngOnInit(): void {
    if (this.tokenService.getToken()) {
      this.roles = this.tokenService.getAuthorities();

      this.roles.every(role => {

        if (role === 'ROlE_ADMIN') {
          this.authority = 'admin';
          return false;
        } else if (role === 'ROlE_PM') {
          this.authority = 'pm';
          return false;
        }

        this.authority = 'user';
        return true;

      });

    }
  }

}
