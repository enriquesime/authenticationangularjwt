import { Component, OnInit } from '@angular/core';
import { TokenService } from '../../auth/token.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private info: any;

  constructor(private tokenService: TokenService) { }

  ngOnInit() {
    this.info = {
      token: this.tokenService.getToken(),
      username: this.tokenService.getUsername(),
      authorities: this.tokenService.getUsername()
    };
  }

  signOut() {
    this.tokenService.signOut();
    window.location.reload();
  }

}
