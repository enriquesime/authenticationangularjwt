import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { AuthSignupInfo } from '../../model/auth-signup-info';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  private form: any = {};
  private signupInfo: AuthSignupInfo;
  private isSignedUp = false;
  private isSignUpFailed = false;
  private errorMessage = '';

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }



  onSubmit() {
    console.log(this.form);

    this.signupInfo = new AuthSignupInfo(this.form.name, this.form.username, this.form.email, this.form.password);

    this.authService.signup(this.signupInfo)
      .subscribe(data => {
        console.log(data);
        this.isSignedUp = true;
        this.isSignUpFailed = false;

      },
        error => {
          console.log(error);
          this.errorMessage = error.error.message;
        });
  }


}
