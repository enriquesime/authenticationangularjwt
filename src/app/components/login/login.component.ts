import { Component, OnInit } from '@angular/core';
import { AuthLoginInfo } from '../../model/auth-login-info';
import { AuthService } from '../../auth/auth.service';
import { TokenService } from '../../auth/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private form: any = {};
  private isLoggedIn = false;
  private isLoginFailed = false;
  private roles: string[] = [];
  private loginInfo: AuthLoginInfo;
  errorMessage = '';

  constructor(private authService: AuthService, private tokenService: TokenService) { }

  ngOnInit() {

    if (this.tokenService.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenService.getAuthorities();
    }
  }

  onSubmit() {
    console.log(this.form);

    this.loginInfo = new AuthLoginInfo(this.form.username, this.form.password);

    this.authService.attemptAuth(this.loginInfo)
      .subscribe(data => {
        this.tokenService.saveToken(data.accessToken);
        this.tokenService.saveUsername(data.username);
        this.tokenService.saveAuthorities(data.authorities);

        this.isLoggedIn = true;
        this.isLoginFailed = false;
        this.reloadPage();

      }, error => {
        console.log(error);
       this.errorMessage = error.error.message;

      });


  }

  reloadPage() {
    window.location.reload();
  }

}
