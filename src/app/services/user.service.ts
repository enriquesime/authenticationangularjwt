import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private baseUserUrl = 'http://localhost:8080/api/test/user';
  private basePmUrl = 'http://localhost:8080/api/test/pm';
  private baseAdminUrl = 'http://localhost:8080/api/test/admin';

  constructor(private http: HttpClient) { }

  getUserBoard(): Observable<string> {
    return this.http.get(this.baseUserUrl, { responseType: 'text' });
  }

  getPmBoard(): Observable<string> {
    return this.http.get(this.basePmUrl, { responseType: 'text' });
  }

  getAdminBoard(): Observable<string> {
    return this.http.get(this.baseAdminUrl, { responseType: 'text' });
  }
}
