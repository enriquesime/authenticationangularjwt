export class AuthSignupInfo {
    private name: string;
    private username: string;
    private email: string;
    private role: string [];
    private password: string;

    constructor(name: string , username: string, email: string , password: string) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.role = ['user'];
        this.password = password;
    }
}
